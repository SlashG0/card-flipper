package com.slashg.cardflipper;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.OvershootInterpolator;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
{

	private static final String KEY_STORED_COUNT = "count";
	int count;
	private TextView cardHalfTop;
	private TextView cardHalfBottom;
	private View cardFull;
	private TextView countDisplay;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		/*
		 * If no savedInstanceState is found, init the counter at 0.
		 * Else, try to init it to the value stored in the Bundle.
		 */
		if (savedInstanceState == null)
		{
			count = 0;
		}
		else
		{
			count = savedInstanceState.getInt(KEY_STORED_COUNT, 0);
		}

		cardHalfBottom = (TextView) findViewById(R.id.flipper_card_bottom);
		cardHalfTop = (TextView) findViewById(R.id.flipper_card_top);
		countDisplay = (TextView) findViewById(R.id.flipper_count_text);

		cardHalfTop.getBackground().setAlpha(0);
		cardHalfBottom.getBackground().setAlpha(0);

		updateCountInView();

	}

	/**
	 * Method call added in XML for this activity.
	 * This method would contain all calls required to initiate
	 * the flip animation
	 *
	 * @param v Unused View param.
	 */
	public void flip(@Nullable View v)
	{

		try
		{

			cardHalfTop.setText(String.valueOf(count++));
			countDisplay.setVisibility(View.GONE);

			ObjectAnimator flipAnimation = ObjectAnimator.ofFloat(cardHalfTop, "rotationX", 0f, -180f);
			flipAnimation.setDuration(500);
			flipAnimation.setInterpolator(new AccelerateDecelerateInterpolator());

			ObjectAnimator flipAnimation2 = ObjectAnimator.ofFloat(cardHalfBottom, "rotationX", -180f, 0f);
			flipAnimation2.setDuration(500);
			flipAnimation2.setInterpolator(new AccelerateDecelerateInterpolator());
//			flipAnimation.addListener(new Animator.AnimatorListener()
//			{
//				@Override
//				public void onAnimationStart(Animator animation)
//				{
//
//				}
//
//				@Override
//				public void onAnimationEnd(Animator animation)
//				{
//					cardHalfTop.setRotationX(0f);
//				}
//
//				@Override
//				public void onAnimationCancel(Animator animation)
//				{
//
//				}
//
//				@Override
//				public void onAnimationRepeat(Animator animation)
//				{
//
//				}
//			});

			ValueAnimator upwardAlphaAnimator = ValueAnimator.ofInt(0,255);
			upwardAlphaAnimator.setDuration(250);
			upwardAlphaAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener()
			{
				@Override
				public void onAnimationUpdate(ValueAnimator animation)
				{
					int alpha = (int) animation.getAnimatedValue();
					cardHalfTop.getBackground().setAlpha(alpha);
					cardHalfBottom.getBackground().setAlpha(alpha);
					Log.d("animator", "Alpha value = " + alpha);
				}
			});

			final ValueAnimator downwardAlphaAnimator = upwardAlphaAnimator.clone();
			downwardAlphaAnimator.setIntValues(255,0);

			upwardAlphaAnimator.addListener(new Animator.AnimatorListener()
			{
				@Override
				public void onAnimationStart(Animator animation)
				{

				}

				@Override
				public void onAnimationEnd(Animator animation)
				{
					cardHalfTop.setText("");
					downwardAlphaAnimator.start();
				}

				@Override
				public void onAnimationCancel(Animator animation)
				{

				}

				@Override
				public void onAnimationRepeat(Animator animation)
				{

				}
			});

			downwardAlphaAnimator.addListener(new Animator.AnimatorListener()
			{
				@Override
				public void onAnimationStart(Animator animation)
				{
					cardHalfBottom.setText(String.valueOf(count));
					countDisplay.setVisibility(View.VISIBLE);
					updateCountInView();
				}

				@Override
				public void onAnimationEnd(Animator animation)
				{

					cardHalfBottom.setText("");
				}

				@Override
				public void onAnimationCancel(Animator animation)
				{

				}

				@Override
				public void onAnimationRepeat(Animator animation)
				{

				}
			});

			upwardAlphaAnimator.start();

			flipAnimation.start();
			flipAnimation2.start();


		} catch (Exception e)

		{
			e.printStackTrace();
		}
	}

	private void updateCountInView()
	{
		if (countDisplay != null)
		{
			countDisplay.setText(String.valueOf(count));
		}
	}

}
